import React, { useState } from 'react';
import { useDebounce } from '../hooks/useDebounce';

export const SearchInput = () => {
  const [value, setValue] = useState('');

  const search = (query) => {
    fetch(`https://jsonplaceholder.typicode.com/todos?query=${query}`)
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
      });
  };

  const debouncedSearch = useDebounce(search, 500);

  const handleChange = (e) => {
    setValue(e.target.value);
    debouncedSearch(e.target.value);
  };

  return (
    <div>
      <input value={value} onChange={handleChange} type="text" />
    </div>
  );
};
