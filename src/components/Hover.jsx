import React, { useRef } from 'react';
import { useHover } from '../hooks/useHover';

export const Hover = ({ width, height, mouseOver, mouseLeave }) => {
  const hoverElement = useRef();
  const isHovering = useHover(hoverElement);

  return (
    <div
      ref={hoverElement}
      style={{
        width: width,
        height: height,
        backgroundColor: isHovering ? mouseOver : mouseLeave,
      }}
    ></div>
  );
};
