import React from 'react';

export const ListItem = ({todo}) => {
  const { id, title, completed } = todo;
  
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        padding: '5px',
        border: '2px solid lightgrey',
      }}
    >
      <p>
        {id}. {title}
      </p>
      <p style={{ backgroundColor: completed ? 'green' : 'red' }}>
        {completed ? 'Выполнено' : 'Не Выполнено'}
      </p>
    </div>
  );
};
