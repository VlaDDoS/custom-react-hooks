import React from 'react'
import { DataService } from '../../API/DataService';
import { useRequest } from '../../hooks/useRequest'
import { ListItem } from './ListItem';

export const ListWithLoading = () => {
  const [todos, loading, error] = useRequest(DataService.getAll());

  if(loading) {
    return (
      <h1>Идёт загрузка...</h1>
    );
  }

  if(error) {
    return (
      <h1>error</h1>
    )
  }

  return (
    <div>
      {todos && todos.map(todo => (
        <ListItem key={todo.id} todo={todo} />
      ))}
    </div>
  )
}
