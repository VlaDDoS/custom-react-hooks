import { ListWithLoading } from './components/List/ListWithLoading';

const App = () => {
  return (
    <div>
      <ListWithLoading />
    </div>
  );
};

export default App;
