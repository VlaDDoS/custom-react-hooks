import { useState } from 'react';

export const useInput = (inputValue) => {
  const [value, setValue] = useState(inputValue);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { value, onChange };
};
