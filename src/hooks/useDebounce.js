import { useCallback, useRef } from 'react';

export const useDebounce = (cb, delay) => {
  const timer = useRef();

  const debouncedInput = useCallback(
    (...args) => {
      if (timer.current) {
        clearTimeout(timer.current);
      }

      timer.current = setTimeout(() => {
        cb(...args);
      }, delay);
    },
    [cb, delay]
  );

  return debouncedInput;
};
