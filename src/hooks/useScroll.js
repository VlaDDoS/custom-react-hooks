import { useEffect, useRef } from 'react';

export const useScroll = (parentRef, childRef, cb) => {
  const observer = useRef();

  useEffect(() => {
    const options = {
      root: parentRef.current,
      rootMargin: '0px',
      threshold: 0,
    };

    if (observer.current) observer.current.disconnect();

    observer.current = new IntersectionObserver(([target]) => {
      if (target.isIntersecting) {
        console.log('Следит');
        cb();
      }
    }, options);

    observer.current.observe(childRef.current);

    return () => {
      observer.current.unobserve(childRef.current);
    };
  }, [cb]);
};
