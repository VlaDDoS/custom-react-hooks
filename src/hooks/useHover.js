import { useEffect, useState } from 'react';

export const useHover = (ref) => {
  const [isHovering, setIsHovering] = useState(false);

  const on = () => setIsHovering(true);
  const off = () => setIsHovering(false);

  useEffect(() => {
    if (!ref.current) {
      return;
    }

    const node = ref.current;

    node.addEventListener('mouseenter', on);
    node.addEventListener('mouseover', on);
    node.addEventListener('mouseleave', off);

    return () => {
      node.removeEventListener('mouseenter', on);
      node.removeEventListener('mouseover', on);
      node.removeEventListener('mouseleave', off);
    };
  }, []);

  return isHovering;
};
