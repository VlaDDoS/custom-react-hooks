export class DataService {
  static getAll(limit = 10, page = 1) {
    return fetch(
      `https://jsonplaceholder.typicode.com/todos?_limit=${limit}&_page=${page}`
    )
    .then(res => res.json());
  }
}
